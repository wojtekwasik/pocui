'use strict';

function EditPage() {
  var tagsAutocomplateParent = element(by.css('.tags-autocomplate'));
  this.tagsInputEl = tagsAutocomplateParent.element(by.model('$mdAutocompleteCtrl.scope.searchText'));
  this.tagsAutocomplateEls = element.all(by.repeater('(index, item) in $mdAutocompleteCtrl.matches'));
  this.selectedTagsEls = element.all(by.repeater('$chip in $mdChipsCtrl.items'));
}
EditPage.prototype.setTagsAutocomplateText = function(text) {
  this.tagsInputEl.sendKeys(text);
}
EditPage.prototype.clearTagsAutocomplateText = function() {
  this.tagsInputEl.clear();
}
EditPage.prototype.getFirstSelectedTagText = function() {
  return this.selectedTagsEls.get(0).element(by.tagName ('strong')).getText();
}
EditPage.prototype.selectAutocomplateTag = function(number) {
  this.tagsAutocomplateEls.get(number).click();
}
EditPage.prototype.removeSelectedTag = function(number) {
  this.selectedTagsEls.get(number).element(by.css('.md-chip-remove')).click();
}

module.exports = new EditPage();
