'use strict';

describe('The edit page view', function () {
  var page;

  beforeEach(function () {
    browser.get('http://localhost:3000/#/edit');
    page = require('./edit-page.po.js');
  });

  it('should display proper number of autocomplete suggestions', function() {
    page.setTagsAutocomplateText('a');
    expect(page.tagsAutocomplateEls.count()).toBe(1);
    page.clearTagsAutocomplateText();
    expect(page.tagsAutocomplateEls.count()).toBe(0);
    page.setTagsAutocomplateText('ne');
    expect(page.tagsAutocomplateEls.count()).toBe(2);

  });
  it('should display correct autocomplate suggestion', function() {
    page.setTagsAutocomplateText('ne');
    expect(page.tagsAutocomplateEls.get(0).getText()).toEqual('news updates');
  });

  it('should add and remove tag', function() {
    page.setTagsAutocomplateText('po');
    page.selectAutocomplateTag(0);
    expect(page.selectedTagsEls.count()).toBe(1);
    expect(page.getFirstSelectedTagText()).toEqual('politics');

    page.removeSelectedTag(0);
    expect(page.selectedTagsEls.count()).toBe(0);

  });

});
