'use strict';

describe('tags controller', function () {
  var scope,
      tagsCtrl;

  beforeEach(module('pocUi'));

  beforeEach(inject(function ($rootScope, $controller) {
    scope = $rootScope.$new();
    tagsCtrl = $controller('TagsCtrl', {
      $scope: scope
    });
  }));

  it('should add and remove tag', inject(function () {
    expect(tagsCtrl.$storage.selectedTags.length).toBe(0);
    expect(tagsCtrl.$storage.tags.length).toBeGreaterThan(0);
    tagsCtrl.addTag();

    var tag = tagsCtrl.selectedItem = tagsCtrl.$storage.tags[1];
    expect(tagsCtrl.$storage.selectedTags.length).toBe(1);
    tagsCtrl.removeTag(tag);
    expect(tagsCtrl.$storage.selectedTags.length).toBe(0);
  }));
});
