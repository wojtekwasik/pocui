'use strict';

module pocUi {

  export class TagsCtrl {
    $storage;
    selectedItem = null;
    searchText: string;
    noCache: boolean = true;

    /* @ngInject */
    constructor(private $localStorage) {

      this.$storage = $localStorage.$default({
        tags: this.loadTags(),
        selectedTags: []
      });

      this.searchText = '';
    }
    addTag() {
      if(this.selectedItem && this.$storage.selectedTags.indexOf(this.selectedItem) === -1) {
        this.$storage.selectedTags.push(this.selectedItem);
      }
      this.searchText = '';
    }

    removeTag(tag) {
      var index = this.$storage.selectedTags.indexOf(tag);
      if(index !== -1) {
        this.$storage.selectedTags.splice(index, 1);
      }
    }

    querySearch(query: string) {
      var results = query ? this.$storage.tags.filter(this.createFilterFor(query) ) : [];
      return results;
    }

    createFilterFor(query: string) {
      var lowercaseQuery = angular.lowercase(query),
          filterFn = (state) => {
            return (state.value.indexOf(lowercaseQuery) === 0) && this.$storage.selectedTags.indexOf(state) === -1;
          }
      return filterFn;
    }

    newTag($chip) {
      if(angular.isString($chip)) return {value: $chip, display: $chip};

      return $chip;

    }

    loadTags() {

      var tags = 'breaking news, boomerbingohawk, world news, interview, happening now, politics, ' +
        'movie news, amc, film, movies, ign, ign news, lifestyle, top headlines, top stories, business news, ' +
        'bollywood gossips, india tv, news updates, nonstop news, latest news, top 100 news, sports news, ' +
        'fatafat news, superfast news, khabarein superfast, 200 news, tel aviv, gaza strip, ' +
        'gaza–israel conflict (military conflict), syria (country), iran, bbc world news (tv network), ' +
        'exit poll, election (quotation subject), middle east (region), israelis (ethnicity), netanayahu';
      return  tags.split(/, +/g).map( function (state) {
        return {
          value: state.toLowerCase(),
          display: state
        };
      });

    }
  }

  angular.module('pocUi').controller('TagsCtrl', TagsCtrl);
}
