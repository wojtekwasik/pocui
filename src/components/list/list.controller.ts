/// <reference path="../../../.tmp/typings/angular-material/angular-material.d.ts" />
/// <reference path="../../services/news/news.service.ts" />

'use strict';

module pocUi {

  export class ListCtrl {

    items;
    currentPage : number = 0;
    totalItems : number;
    maxSize = 10;

    /* @ngInject */
    constructor(private newsService: NewsService, private $mdDialog: angular.material.MDDialogService) {
      this.fetchData();
    }

    pageChanged() {
      this.fetchData();
    }

    fetchData() {
      this.newsService.getNews(this.currentPage, 10)
        .success((respone) => {
          this.items = respone.Items;
          this.currentPage = respone.Page;
          this.totalItems = respone.Count;
        })
        .error(() => {
          this.$mdDialog.show(
            this.$mdDialog.alert()
              .title('Error')
              .content('unable to connect to server ')
              .ok('Ok')
          )
        })
    }
  }

  angular.module('pocUi.list', []).controller('ListCtrl', ListCtrl);
}
