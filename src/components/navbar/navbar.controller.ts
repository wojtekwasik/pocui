'use strict';

module pocUi {

  export class NavbarCtrl {

    /* @ngInject */
    constructor (private $mdSidenav) {}

    togggleSlidenav() {
      this.$mdSidenav('sidenav').toggle();
    }

    toggleSearch() {
      this.$mdSidenav('sidenav-right').toggle();
    }
  }

  angular.module('pocUi').controller('NavbarCtrl', NavbarCtrl);
}
