'use strict';

module pocUi {

  export class ContactsCtrl {

    items;

    /* @ngInject */
    constructor () {
      this.items = [{
        "id": 0,
        "name": "Aurelia Everett",
        "email": "aureliaeverett@boink.com",
        "address": "647 Central Avenue, Axis, North Dakota, 6426"
      },
        {
          "id": 1,
          "name": "Cherie Rosario",
          "email": "cherierosario@boink.com",
          "address": "313 Mill Street, Evergreen, Mississippi, 649"
        },
        {
          "id": 2,
          "name": "Maddox Ayers",
          "email": "maddoxayers@boink.com",
          "address": "524 Manhattan Avenue, Hollins, American Samoa, 1259"
        }]
    }
  }

  angular.module('pocUi').controller('ContactsCtrl', ContactsCtrl);
}
