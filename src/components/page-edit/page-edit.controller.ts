'use strict';

module pocUi {

  export class PageEditCtrl {

    dropItems = [];
    dragOptions;

    constructor(private $mdDialog:angular.material.MDDialogService) {

      this.dragOptions = {
        accept: function (sourceItemHandleScope, destSortableScope) {
          return true
        },
        itemMoved: function (event) {
        },
        orderChanged: function (event) {
        }
      }

    }

    publish() {
      var alert = this.$mdDialog.alert()
        .content('This is an example of how easy dialogs can be!')
        .title('Attention')
        .ok('Close');


      this.$mdDialog.show(alert).finally(function () {
        alert = undefined;
      });
    }
  }

  angular.module('pocUi.page-edit', []).controller('PageEditCtrl', PageEditCtrl);
}
