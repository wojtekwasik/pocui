module pocUi {

  export class SearchCtrl {

    selectedItem:any = null;
    searchText:string = null;
    noCache:boolean = true;
    items;
    request:any = null;

    /*ngInject*/
    constructor(private $scope:angular.IScope, private esClient, private $q:angular.IQService) {
      this.init();
    }

    querySearch(query:string) {
      if (this.request) {
        this.request.abort();
      }
      var deferred = this.$q.defer(),
        queryObj = {};

      if (query.indexOf('like:') !== -1) {
        var s = query.split(':');
        queryObj = {
          more_like_this: {
            fields: [
              'contentpage.headline',
              'contentpage.body'
            ],
            docs: [
              {
                _index: "newsbank",
                _type: "news",
                _id: s[1]
              }
            ],
            "min_term_freq": 1,
            "max_query_terms": 12
          }
        }
      } else {
        queryObj = {
          match: {
            _all: query
          }
        }
      }

      this.request = this.esClient.search({
        index: 'newsbank',
        body: {
          query: queryObj
        }
      });

      this.request.then((body) => {
        this.items = body.hits.hits;
      }, (error) => {
        console.error(error);
      }).finally(() => {
        deferred.resolve([]);
        this.request = null;
      });

      return deferred.promise;
    }

    private init() {
      this.$scope.$watch('search.searchText', (newValue) => {
        if (!newValue) {
          this.items = [];
        }
      });
    }

  }

  angular.module('pocUi.search', []).controller('SearchCtrl', SearchCtrl);

  angular.module('pocUi.search').filter('htmlToPlaintext', function () {
    return function (text) {
      return String(text).replace(/<[^>]+>/gm, '');
    }
  });
}
