'use strict';

module pocUi {

  export class SidenavCtrl {

    /* @ngInject */
    constructor(private $mdSidenav, private $router) {

    }
    toggleRight() {
      this.$mdSidenav('sidenav').toggle();
    }
    navigate(component) {
      this.$router.navigate(component)
      this.$mdSidenav('sidenav').close();

    }
  }
  angular.module('pocUi').controller('SidenavCtrl', SidenavCtrl);
}
