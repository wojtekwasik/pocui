'use strict';

((): void => {

  angular.module('pocUi', [
    'ngAnimate',
    'ngMaterial',
    'textAngular',
    'placeholders.img',
    'placeholders.txt',
    'ngStorage',
    'ngNewRouter',
    'pocUi.list',
    'ui.bootstrap.pagination',
    'pocUi.page-edit',
    'pocUi.search',
    'pocUi.page-edit-aside',
    'elasticsearch',
    'ui.sortable'
  ]).config(
    /*@ngInject*/
    function($mdIconProvider, $componentLoaderProvider) {

      var DEFAULT_SUFFIX = 'Ctrl';

      $componentLoaderProvider.setCtrlNameMapping(function(name) {
        return name[0].toUpperCase() + name.substr(1) + DEFAULT_SUFFIX;
      });

      $componentLoaderProvider.setComponentFromCtrlMapping(function ctrlToComponentDefault(name) {
        return name[0].toLowerCase() + name.substr(1, name.length - DEFAULT_SUFFIX.length - 1);
      });

      $componentLoaderProvider.setTemplateMapping(function componentToTemplateDefault(name) {
        var dashName = dashCase(name);
        return 'components/' + dashName + '/' + dashName + '.html';
      });

      function dashCase(str) {
        return str.replace(/([A-Z])/g, function ($1) {
          return '-' + $1.toLowerCase();
        });
      }

      $mdIconProvider
        .icon('menu1','/assets/ic_menu_24px.svg', 24)
        .icon('search','/assets/ic_search_24px.svg', 24)
        .icon('person', '/assets/ic_person_24px.svg', 24)
        .icon('save', '/assets/ic_save_24px.svg', 24)
        .icon('pageview', '/assets/ic_pageview_24px.svg', 24)
        .icon('edit', '/assets/ic_edit_24px.svg', 24)
        .icon('close', '/assets/ic_close_24px.svg', 24)
        .icon('viewlist', '/assets/ic_view_list_24px.svg', 24);
    });

})();
