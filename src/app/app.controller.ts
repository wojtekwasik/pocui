'use strict';

module pocUi {

  export class AppCtrl {

    /* @ngInject */
    constructor ($router, private $mdSidenav) {
      $router.config([
        { path: '/', redirectTo: '/edit' },
        { path: '/edit',components: {main: 'pageEdit', aside:'pageEditAside'}},
        { path: '/list',components: {main: 'list', aside: 'listAside'}},
        { path: '/search',components: {main: 'search', aside: 'listAside'}}
      ]);
    }
  }

  angular.module('pocUi').controller('AppCtrl', AppCtrl);
}
