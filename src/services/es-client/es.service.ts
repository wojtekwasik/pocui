/// <reference path="../../../.tmp/typings/angularjs/angular.d.ts" />
'use strict';

module pocUi {

  angular.module('pocUi')
    .service('esClient', ['esFactory',function (esFactory) {
      return esFactory({
        host: '10.9.44.37:9200'
      });
    }])
}
