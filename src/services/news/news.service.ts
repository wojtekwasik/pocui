/// <reference path="../../../.tmp/typings/angularjs/angular.d.ts" />
'use strict';

module pocUi {

  interface IPagedListResult<T> {
    HasNext: boolean;
    HasPrevious: boolean;
    Page: number;
    Size: number;
    Count: number;
    Items: IList<T>;
  }

  interface IList<T> {

  }
  interface IDominoNews {
    Unid: string;
    Form: string;
    SecureResource: string;
    PageAuthor: string;
    Abstract: string;
    ArticleCountry: string;
  }


  export class NewsService {

    static API_URL = 'http://localhost:56384/api';

    /* @ngInject */
    constructor(private $http: angular.IHttpService, $q) {}

    getNews(page: Number, size: Number) : angular.IHttpPromise<IPagedListResult<IDominoNews>> {
      return this.$http.get(NewsService.API_URL + '/infos.json', {params: {page: page, size: size}});
    }

  }

  angular.module('pocUi').service('newsService', NewsService);
}
